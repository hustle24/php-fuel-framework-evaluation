'use strict';

var angularApp = angular.module('angularApp', []);

angularApp.controller('AngularCtrl', ['$scope', '$http',
	function($scope, $http) {

		$scope.refresh = function() {
			$http.get('//localhost:8888/v1/api/news.json', {
				'params': {
					'token': 'VALIDTOKEN'
				}
			}).success(function(data) {
				$scope.articles = data;
			});
		}

		$scope.refresh();

		$scope.update = function(news) {
			$http({
				method: 'POST',
				url: '//localhost:8888/v1/api/news.json',
				data: news
			}).success(function(data, status, headers, config) {
				$scope.refresh();
			}).error(function(data, status, headers, config) {
				console.log(data);
			});
		};

		$scope.delete = function(id) {
			$http.delete('//localhost:8888/v1/api/news.json', {
				'data': {
					'id': id
				}
			}).success(function(data) {
				$scope.refresh();
			});
		}
	}
]);
