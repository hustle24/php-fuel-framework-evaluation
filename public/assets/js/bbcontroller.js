var NewsModel = Backbone.Model.extend({
	urlRoot: '//localhost:8888/v1/api/news.json?token=VALIDTOKEN'
});

var NewsCollection = Backbone.Collection.extend({
	model: NewsModel,
	url: '//localhost:8888/v1/api/news.json?token=VALIDTOKEN'
});

var News = new NewsCollection();

var AppView = Backbone.View.extend({
	initialize: function() {
		//this.listenTo(News, 'sync', this.addAll);
		this.listenTo(News, 'add', this.addOne);
		News.fetch();
	},
	addAll: function() {
		News.each(this.addOne, this);

	},
	addOne: function(news) {
		$(".articles").append("<li>" + news.attributes.title + ":" + news.attributes.author + "</li>");
	}
});

var app = new AppView();

var FormView = Backbone.View.extend({
	el: '.simple-form',

	events: {
		"submit": "submit",
	},

	initialize: function() {
		console.log("initialize");
	},

	submit: function(e) {
		e.preventDefault();
		var model = new NewsModel();
		this.$el.find('input').each(function() {
			model.set(this.name, this.value);
		});
		model.save({}, {
			success: function(model, response) {
				News.fetch();
			},
			error: function(model, response) {
				console.log(response);
			}
		});
	}
});

new FormView();
