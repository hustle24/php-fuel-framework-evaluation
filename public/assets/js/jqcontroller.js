$(document).ready(function(){

	$.ajax({
		url: '//localhost:8888/v1/api/news.json',
		type: "GET",
		dataType: "json",
		data: {'token': 'VALIDTOKEN'},
		success: function(data) {
			$.each(data,function(index,value){
 				$(".articles").append("<li>" + value.title + ":" + value.author + "</li>");
			})
		}
	});
	
	$("button").click(function(){
		var title = $("#title").val();
		var author =$("#author").val()
		
		$.ajax({
			url: '//localhost:8888/v1/api/news.json',
			type: "POST",
			dataType: "json",
			contentType: 'application/json; charset=utf-8',
			// data: 'title=' + title + '&author=' + author ,
			data: JSON.stringify({'title': title, 'author': author}),
			success: function(data) {
				$(".articles").append("<li>" + data.title + ":" + data.author + "</li>");
			}
		});
	});
});