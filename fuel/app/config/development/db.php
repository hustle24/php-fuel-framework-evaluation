<?php
/**
 * The development database settings. These get merged with the global settings.
 */

return array(
	'default' => array(
		'connection'  => array(
			'dsn'        => 'mysql:host=localhost;dbname=core3_dev',
			'port' => '8889',
			'username'   => 'root',
			'password'   => 'root',
		),
	),
);
