<?php
namespace Fuel\Core; 

class Tests_Api extends TestCase{
	
	public function test_get_example(){
		$response = Request::forge('v1/api/example',true)->set_method('GET')->execute(array('foo' => 'bar'))->response();
		$this->assertEquals($response->status, 200);
		$json = json_decode($response->body());
		$this->assertEquals($json->foo, 'bar');
	}
	
	public function test_post_news(){
		$response = Request::forge('v1/api/news',true)->set_method('POST')->execute(array('title' => "UNIT_TEST"
			,'author' => "UNIT_TEST"))->response();
		$this->assertEquals($response->status, 200);
	}
	
	public function test_get_news(){
		$response = Request::forge('v1/api/news',true)->set_method('GET')->execute(array('token' => 'VALIDTOKEN'))->response();
		$this->assertEquals($response->status, 200);
		$json = json_decode($response->body());
		$this->assertEquals($json[0]->title, 'UNIT_TEST');
	}
}
?>