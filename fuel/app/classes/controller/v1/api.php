<?php

use \Model\News;
use \Model\OAuth;

class Controller_v1_Api extends Controller_Rest{
	
	public $request = null;
	public $db = null;
	public $server = null;
		
	public function get_example($foo = null){
		return $this->response(array(
			'foo' => Input::get('foo') ? Input::get('foo') : $foo ,
			'baz' => array(
				1, 50, 219
			),
			'empty' => null
		));
	}
		
	public function get_news($token = null )
	{	
		$_token = "";
		$input = file_get_contents("php://input");
		$json_post = json_decode($input,true);
		
		if ($input && $json_post){ 
			$_token = $json_post['token'] ;
		}else{
			$_token = Input::get('token') ? Input::get('token') : $token ;
		}
	
		if(Oauth::isValidToken($_token)){
			$resultsArray = array();
			$results = News::query()->select('title', 'author')->get();
			foreach($results as $result){
				$resultsArray[] = $result;
			}
		}else{
			$resultsArray[] = array( 'error' => "Invalid or missing token.") ;
		}
			
		return $resultsArray;
	}
	
	public function post_news($title = null, $author = null){
		$news = new News();
		$input = file_get_contents("php://input");
		$json_post = json_decode($input,true);
		if ($input && $json_post){ 
			$news->title = $json_post['title'] ;
			$news->author = $json_post['author'];
		}else{ 
			$news->title = Input::post('title') ? Input::post('title') : $title ;
			$news->author = Input::post('author') ? Input::post('author') : $author;
		} 
		
		
		$news->save();
		
		$resposne = array('id' => $news->id, 'title' => $news->title, 'author' => $news->author ) ;
		$json = json_encode($resposne);
		return Response::forge($json ,200);
	}
	
	public function delete_news($id = null){
		$_id = "";
		
		$input = file_get_contents("php://input");
		$json_post = json_decode($input,true);
		
		if ($input && $json_post){ 
			$_id = $json_post['id'] ;
		}else{
			$_id = Input::get('id') ? Input::get('id') : $id ;
		}
		
		$news = News::find($_id);
		$news->delete();

		return Response::forge('{}' ,200);
		}
	}
	?>