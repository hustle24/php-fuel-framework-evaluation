<?php
	
class Controller_Main extends Controller
{
	public function action_index()
	{
		$view = View::forge('main/index');
		$response = Request::forge('v1/api/news','curl')->set_method('GET')->execute(array('token' => 'VALIDTOKEN'))->response();
		$view->set('articles', $response->body()['item']);
		$view->footer = View::forge('widgets/footer', array('site_title' => 'My Website'));
		return $view;
	}
	
	public function action_submit()
	{
		$response = Request::forge('v1/api/news','curl')->set_method('POST')->execute(array('title' => Input::post('title')
			,'author' => Input::post('author')))->response();
		Response::redirect_back('/index', 'refresh');
	}
}
?>